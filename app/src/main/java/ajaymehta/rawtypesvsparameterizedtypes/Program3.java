package ajaymehta.rawtypesvsparameterizedtypes;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

public class Program3 {


    public static void rawTypes() {


        List names = Arrays.asList("one","two","three","four","five","six");
        List numbers = Arrays.asList(1,2,3,4,5,6,7,8,9);

        // we dont have to cast

        // here we put Object ..coz for each loop takes a single element at a time ..so we have List and single element of list is ...names[0] - > that is Object  ..at each iteration..
        for(Object i : names) {

            System.out.println(i);
        }

        System.out.println("====================");

        for(Object i : numbers) {

            System.out.println(i);
        }




    }




    public static void parameterizedTypes() {

        List<Integer> numbers = Arrays.asList(1,2,3,4,5,6,7,8,9);

        for(Integer i : numbers) {  // by taking above paramentorze type List<Integer> ..here we could take Integer instead of Object...

            System.out.println(i);
        }

    }





    public static void main(String args[]) {

        rawTypes();
        parameterizedTypes();
    }
}
