package ajaymehta.rawtypesvsparameterizedtypes;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

public class Program2 {


    public static void rawTypes() {


        List names = Arrays.asList("one","two","three","four","five","six");
        List numbers = Arrays.asList(1,2,3,4,5,6,7,8,9);


        // with for loop ..we dont have to cast ..because for loop is just looping through ( List ) n we getting list elements n printing them..

      for(int i=0 ; i< names.size(); i++) {

          System.out.println(names.get(i));
      }

        System.out.println("=======================");

        for(int i=0 ; i< numbers.size(); i++) {

            System.out.println(numbers.get(i));
        }

        System.out.println("======================");


    }




    public static void parameterizedTypes() {

        // its all same in raw and in paramiterized ..type ..no change...

        List<Integer> counting = Arrays.asList(11,22,33,44,55,66,77,88,99);


        for(int i=0 ; i< counting.size(); i++) {

            System.out.println(counting.get(i));
        }


    }





    public static void main(String args[]) {

        rawTypes();

        parameterizedTypes();
    }
}
