package ajaymehta.rawtypesvsparameterizedtypes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

// Parementerized type and raw types behave same ... till it comes to looping through data...

// While looping though raw elements of List using iterator ..you have to cast (with Object type you don;t have to ) ..to tell what type of elements you are looping through..but with for loop ..you dont have to..it print any time of data..

public class Program1 {


    public static void rawTypes() {  // raw type means we are not telling what type of objects our list has got like List<String>

        //Arrays.asList ( asList method of Arrays class -- > convert array into List ( objects ) .
        List names = Arrays.asList("one","two","three","four","five","six"); // asList is genric Type (T) ..it takes any type of array data..
        List numbers = Arrays.asList(1,2,3,4,5,6,7,8,9);

        //now attach itertor to list
        Iterator iterator = names.iterator();

        while(iterator.hasNext()) { // loop till we have elements in the list (return boolean)
            // we have to cast --> to tell what type of elemnets we have in our list ..
            String elements = (String) iterator.next(); //starting the list ..will to got next element in loop
            System.out.println(elements);


        } // End of While Loop...
        System.out.println("===============================");


            Iterator itee =  numbers.iterator();  // adding iterators to numbers...

            while (itee.hasNext()) {

              //   Integer elements2 = (Integer) itee.next();  //  you can use it or below ..both are okay ..u just has to cast here..

                // when it terms to printin ..u can print by taking Object element2 too..both are good way to do it..
                 Object elements2 =  itee.next();

                System.out.println(elements2);
            }


        }




    public static void parameterizedTypes() {  //same as Raw types in terms of Printing..

        List<String> counting = Arrays.asList("one1","two2","three3","four4","five5","six6");

        Iterator iterator =  counting.iterator();  // adding iterators to numbers...

        while (iterator.hasNext()) {

            String data = (String)iterator.next();

            System.out.println(data);
        }


    }

    public static void main(String args[]) {

        rawTypes();

        System.out.println("===========================");
       parameterizedTypes();
    }
}


// Reason why not to use Raw types ..

/*
    Raw types refer to using a generic type without specifying a type parameter. For example, List is a raw type, while List<String> is a parameterized type.
        When generics were introduced in JDK 1.5, raw types were retained only to maintain backwards compatibility with older versions of Java. Although using raw types is still possible, they should be avoided:

        they usually require casts
        they aren't type safe, and some important kinds of errors will only appear at runtime
        they are less expressive, and don't self-document in the same way as parameterized types*/
