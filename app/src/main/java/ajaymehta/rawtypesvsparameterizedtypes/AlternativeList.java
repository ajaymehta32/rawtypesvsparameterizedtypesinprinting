package ajaymehta.rawtypesvsparameterizedtypes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Avi Hacker on 7/16/2017.
 */

public class AlternativeList {


    public static void rawTypes() {


        // the last we took previous ...n here we taking arraylist ...both are difference... arraylist is subclass of List
    // TODO ..uncommnent n see ..it gives error...
     //   ArrayList example = Arrays.asList(1,2,3,4,5,6,7,8,9);

// we took arrary list here ..coz List is an interface ..we can directly use that n put elements in that..
       List names = new ArrayList();
        names.add("One"); names.add("Two"); names.add("three"); names.add("four"); names.add("five"); names.add("six");

        List numbers = new ArrayList();

        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);
        numbers.add(6);
        numbers.add(7);


        for(Object i : names) {

            System.out.println(i);
        }

        System.out.println("====================");

        for(Object i : numbers) {

            System.out.println(i);
        }




    }


    public static void parameterizedTypes() {


    }





    public static void main(String args[]) {

        rawTypes();
        parameterizedTypes();
    }
}
